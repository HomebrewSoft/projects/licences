{
    "name": "Contract Licence",
    "version": "16.0.0.1.0",
    "author": "HomebrewSoft",
    "website": "https://homebrewsoft.dev",
    "license": "OPL-1",
    "depends": [
        "contract",
        "sale",
    ],
    "data": [
        # security
        "security/ir.model.access.csv",
        # data
        "data/ir_cron.xml",
        # reports
        # views
        "views/contract_contract.xml",
        "views/contract_licence_manager.xml",
        "views/contract_line.xml",
        "views/product_template.xml",
    ],
}
