import json
import logging

import requests
from odoo import _, fields, models
from odoo.exceptions import ValidationError

_logger = logging.getLogger(__name__)
TIMEOUT = 5


class ContractLicenceManager(models.Model):
    _name = "contract.licence_manager"
    _description = "Contract Licence Manager"

    name = fields.Char(
        required=True,
    )
    username = fields.Char()
    password = fields.Char()
    webhook_url = fields.Char(
        required=True,
    )
    auth_url = fields.Char()
    sync_source_url = fields.Char()
    user_creation_url = fields.Char()
    partner_ids = fields.One2many(
        comodel_name="contract.licence_manager.partner",
        inverse_name="licence_manager_id",
    )

    def get_partner_external_id(self, partner):
        if not self.sync_source_url or not self.username:
            _logger.warning("No sync data not complete manager %s", self.name)
            return
        """Get the external id of a partner"""
        external_partner = self.partner_ids.filtered(lambda p: p.partner_id == partner)
        if not external_partner:
            external_partner = self._create_external_user(partner)
        return external_partner[0].external_identifier

    def sync_source(self, external_id, source_name):
        if not self.sync_source_url or not self.password:
            _logger.warning("No sync the config is not complete: manager %s", self.name)
            return
        headers = self._get_headers()
        res = requests.put(
            self.sync_source_url,
            headers=headers,
            data=json.dumps({"source": source_name, "user_id": external_id}),
            timeout=TIMEOUT,
        )
        return res

    def _create_external_user(self, partner):
        if not self.sync_source_url  or not self.password:
            _logger.warning("No sync source url defined for the manager %s", self.name)
            return
        """Create an user in the external application"""
        _logger.info("Creating an user for the partner %s", partner.id)
        body = {
            "name": partner.name,
            "email": partner.email,
        }
        headers = self._get_headers()
        response = requests.post(self.user_creation_url, headers=headers, data=json.dumps(body))
        if response.status_code != 200:
            _logger.error(response.text)
            raise ValidationError(_("Error while creating the user in the external application"))
        return self.env["contract.licence_manager.partner"].create(
            {
                "partner_id": partner.id,
                "external_identifier": response.json()["id"],
                "licence_manager_id": self.id,
            }
        )

    def _get_license_data(self, contract: "contract.contract"):
        """Get the request body to sent to the webhook"""
        license_data = {
            "id": 1,  # TODO get the id from the external app  # pylint: disable=fixme
            "user_id": self.get_partner_external_id(contract.partner_id),
            "date_start": contract.date_start.isoformat() if contract.date_start else None,
            "date_end": contract.date_end.isoformat() if contract.date_end else None,
            "details": json.loads(contract.license_details),
            "stripe_status": contract.stripe_status or None,
        }
        return {"licenses": [license_data]}

    def _auth(self):
        if not self.sync_source_url or not self.username:
            _logger.warning("No sync data not complete manager %s", self.name)
            return
        """Authenticate to the webhook"""
        _logger.info("Authenticating by the manager %s", self.name)
        # TODO make more flexible  # pylint: disable=fixme
        body = {
            "flow": "USER_PASSWORD_AUTH",
            "params": {
                "USERNAME": self.username,
                "PASSWORD": self.password,
            },
        }
        headers = {
            "Content-Type": "application/json",
        }
        response = requests.post(self.auth_url, headers=headers, data=json.dumps(body))
        if response.status_code != 200:
            _logger.error(response.text)
            raise ValidationError(_("Error while authenticating to the external app"))
        return response.json()["AccessToken"]

    def _get_headers(self):
        """Get the headers to send to the webhook"""
        headers = {
            "Content-Type": "application/json",
        }
        if self.auth_url:
            token = self._auth()
            headers["access_token"] = token
        return headers

    def sync_contract(self, contract: "contract.contract"):
        """Send the licenses data to the webhook"""
        if not self.webhook_url or not self.password:
            _logger.warning("No webhook url or password defined for the manager %s", self.name)
            return
        _logger.info(
            "Sending contract %s info by the manager %s",
            contract.ids,
            self.name,
        )
        headers = self._get_headers()
        body = self._get_license_data(contract)
        _logger.warning(body)
        response = requests.put(self.webhook_url, headers=headers, data=json.dumps(body))
        if response.status_code != 200:
            _logger.error(response.text)
            raise ValidationError(_("Error while sending the license data to the webhook"))
