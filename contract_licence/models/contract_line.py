from odoo import api, fields, models


class ContractLine(models.Model):
    _inherit = "contract.line"

    partner_id = fields.Many2one(
        related="contract_id.partner_id",
        store=True,
    )
    licence_details = fields.Text()

    def create(self, vals_list):
        res = super(ContractLine, self).create(vals_list)
        for line in res:
            line.licence_details = line.product_id.licence_details
        return res

    @api.depends("product_id.licence_details")
    def _compute_licence_details(self):
        for line in self:
            if line.product_id:
                line.licence_details = line.product_id.licence_details

    def _prepare_invoice_line(self, move_form):
        res = super(ContractLine, self)._prepare_invoice_line(move_form)
        res["sale_line_ids"] = [(4, self.contract_id.sale_order_line_id.id)]
        return res
