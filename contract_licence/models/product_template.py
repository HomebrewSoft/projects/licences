import json

from odoo import api, fields, models
from odoo.exceptions import ValidationError


class ProductTemplate(models.Model):
    _inherit = "product.template"

    is_licence = fields.Boolean()
    default_licence_manager_id = fields.Many2one(
        comodel_name="contract.licence_manager",
        string="Default manager",
    )
    licence_details = fields.Text(
        help="JSON with the Licence details",
        string="Details",
    )
    licence_auto_renew = fields.Boolean(
        string="Auto renew",
    )
    licence_recurring_interval = fields.Integer(
        string="Recurring interval",
    )
    licence_recurring_rule_type = fields.Selection(
        selection=[
            ("daily", "Day(s)"),
            ("weekly", "Week(s)"),
            ("monthly", "Month(s)"),
            ("monthlylastday", "Month(s) last day"),
            ("quarterly", "Quarter(s)"),
            ("semesterly", "Semester(s)"),
            ("yearly", "Year(s)"),
        ],
        default="monthly",
        string="Recurrence",
        help="Specify Interval for automatic invoice generation.",
    )
    licence_recurring_invoicing_type = fields.Selection(
        selection=[
            ("pre-paid", "Pre-paid"),
            ("post-paid", "Post-paid"),
        ],
        default="pre-paid",
        string="Invoicing type",
        help=(
            "Specify if the invoice must be generated at the beginning "
            "(pre-paid) or end (post-paid) of the period."
        ),
    )

    @api.constrains("licence_details")
    def _check_(self):
        for record in self:
            if record.licence_details:
                try:
                    json.loads(record.licence_details)
                except json.decoder.JSONDecodeError as e:
                    raise ValidationError("Invalid JSON in licence_details") from e
