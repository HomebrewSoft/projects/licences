import json
import logging
from typing import Any, Dict

from odoo import api, fields, models

_logger = logging.getLogger(__name__)


class ContractContract(models.Model):
    _inherit = "contract.contract"

    licence_manager_id = fields.Many2one(
        comodel_name="contract.licence_manager",
    )
    sale_order_line_id = fields.Many2one(
        comodel_name="sale.order.line",
    )
    license_details = fields.Text(
        compute="_compute_license_details",
        store=True,
    )
    last_sync_date = fields.Datetime(
        readonly=True,
    )
    last_license_details_update = fields.Datetime(
        readonly=True,
    )
    need_sync = fields.Boolean(
        compute="_compute_need_sync",
        store=True,
    )


    @api.depends("last_sync_date", "last_license_details_update")
    def _compute_need_sync(self):
        for contract in self:
            if not (contract.last_sync_date and contract.last_license_details_update):
                contract.need_sync = True
                return
            contract.need_sync = contract.last_sync_date < contract.last_license_details_update
            if contract.need_sync:
                contract.sync()

    @api.depends(
        "contract_line_fixed_ids.licence_details",
        "contract_line_fixed_ids.product_id.default_code",
        "contract_line_fixed_ids.quantity",
        "contract_line_fixed_ids",
        "date_end",
    )
    def _compute_license_details(self):
        MAX_VALUE = "unlimited"
        for record in self:
            details = {
                "max_emails_enroll": 0,
                "max_companies": 0,
                "exceed_metadata_limit": False,
                "add_enabled": False,
                "products": [],
            }
            for line in record.contract_line_fixed_ids:
                line_details = json.loads(line.licence_details or "{}")
                self._add_property(
                    details, "max_emails_enroll", line_details, MAX_VALUE, line.quantity
                )
                self._add_property(details, "max_companies", line_details, MAX_VALUE, line.quantity)
                if line_details.get("exceed_metadata_limit"):
                    details["exceed_metadata_limit"] = True
                if line_details.get("add_enabled"):
                    details["add_enabled"] = True

                details["products"].append(
                    {
                        "identifier": line.product_id.default_code,
                        "quantity": line.quantity,
                    }
                )
            record.license_details = json.dumps(details)
            record.last_license_details_update = fields.Datetime.now()

    def _add_property(
        self, details: Dict[str, Any], key, new_dict, max_value: str, multiplier: int
    ) -> None:
        if details[key] == max_value:
            return
        new_value = new_dict.get(key, 0)
        if new_value == max_value:
            details[key] = max_value
        else:
            new_value = int(new_value) * multiplier
            details[key] += new_value

    def sync(self):
        _logger.info("Syncing contracts %s", self)
        for contract in self:
            if not contract.need_sync:
                _logger.info("Contract %s does not need to be synced", contract.id)
                continue
            if not contract.licence_manager_id:
                _logger.warning("Contract %s does not have a licence manager", contract.id)
                continue
            if not contract.contract_line_fixed_ids:
                _logger.warning("Contract %s does not have any lines", contract.id)
                continue
            _logger.info("Syncing contract %s", contract.id)
            contract.licence_manager_id.sync_contract(contract)
            contract.last_sync_date = fields.Datetime.now()
            contract.need_sync = False

    @api.model
    def cron_sync_licences(self):
        self.search(["need_sync", "=", True]).sync()

    def manual_sync(self):
        for contract in self:
            contract.need_sync = True
            contract.sync()

    def create_from_external(
        self, name, partner, product, price, licence_manager
    ):  # pylint: disable=too-many-arguments
        date_end = self.env["contract.line"]._get_first_date_end(
            fields.Date.today(),
            product.licence_recurring_rule_type,
            product.licence_recurring_interval,
        )
        # TODO  make more flexible in times # pylint: disable=fixme
        return self.create(
            {
                "name": name,
                "partner_id": partner.id,
                "licence_manager_id": licence_manager.id,
                "contract_line_fixed_ids": [
                    (
                        0,
                        None,
                        {
                            "product_id": product.id,
                            "name": product.get_product_multiline_description_sale(),
                            "price_unit": price,
                            "quantity": 1,
                            "is_auto_renew": product.licence_auto_renew,
                            "auto_renew_interval": product.licence_recurring_interval,
                            "auto_renew_rule_type": product.licence_recurring_rule_type,
                            "recurring_interval": product.licence_recurring_interval,
                            "recurring_rule_type": product.licence_recurring_rule_type,
                            "termination_notice_interval": 1,
                            "termination_notice_rule_type": "weekly",
                            "date_end": date_end,
                        },
                    ),
                ],
            }
        )
