from odoo import fields, models


class ContractLicenceManagerPartner(models.Model):
    _name = "contract.licence_manager.partner"
    _description = "Contract Licence Manager Partner"

    partner_id = fields.Many2one(
        comodel_name="res.partner",
        required=True,
        index=True,
    )
    external_identifier = fields.Char(
        required=True,
        index=True,
    )
    licence_manager_id = fields.Many2one(
        comodel_name="contract.licence_manager",
        required=True,
        index=True,
    )
