from odoo.addons.contract.tests.test_contract import TestContractBase


class TestContract(TestContractBase):
    @classmethod
    def setUpClass(cls):  # pylint: disable=invalid-name
        super(TestContract, cls).setUpClass()

        cls.product_4 = cls.product_1.copy()
        cls.product_4.type = "service"
        cls.product_4.is_license = True

        cls.contract4 = cls.contract2.copy()
        cls.contract4.contract_line_ids[0].product_id = cls.product_4

    def test_sync(self):
        self.contract.contract_line_ids.sync()
