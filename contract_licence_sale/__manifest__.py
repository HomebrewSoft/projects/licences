{
    "name": "Contract Licence Sale",
    "version": "16.0.0.1.0",
    "author": "HomebrewSoft",
    "website": "https://homebrewsoft.dev",
    "license": "OPL-1",
    "depends": [
        "contract_licence",
        "sale",
        "utm",
    ],
    "data": [
        # security
        # data
        # reports
        # views
        "views/contract_contract.xml",
        "views/res_partner.xml",
        "views/sale_order.xml",
        "views/utm_source.xml",
    ],
}
