from odoo import fields, models


class UTMSource(models.Model):
    _inherit = "utm.source"

    pricelist_id = fields.Many2one(
        comodel_name="product.pricelist",
    )
