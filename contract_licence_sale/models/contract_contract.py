import logging
from odoo import api, fields, models

_logger = logging.getLogger(__name__)


class ContractContract(models.Model):
    _inherit = "contract.contract"

    source_id = fields.Many2one(
        comodel_name="utm.source",
        related="partner_id.source_id",
    )
    pricelist_id = fields.Many2one(
        related="source_id.pricelist_id",
    )
    source_updated = fields.Boolean(
        compute="_compute_source_updated",
        store=True,
    )

    @api.onchange("partner_id", "company_id")
    def _onchange_partner_id(self):
        super()._onchange_partner_id()
        self.pricelist_id = self.source_id.pricelist_id


    @api.depends("source_id", "source_id.name", "partner_id")
    def _compute_source_updated(self):
        for contract in self:
            if not self.licence_manager_id.username or not self.licence_manager_id.password:
                _logger.warning("No username or password defined for the manager %s", self.name)
                return
            if not contract.licence_manager_id:
                continue
            contract.source_updated = False
            external_id = contract.licence_manager_id.get_partner_external_id(contract.partner_id)
            source_name = contract.source_id and contract.source_id.name or ""
            res = contract.licence_manager_id.sync_source(
                external_id, source_name
            )
            contract.source_updated = True
