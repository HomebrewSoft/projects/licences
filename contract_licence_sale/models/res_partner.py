from odoo import fields, models


class Partner(models.Model):
    _inherit = "res.partner"

    source_id = fields.Many2one(
        comodel_name="utm.source",
        string="Fuente",
    )
