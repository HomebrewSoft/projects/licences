from odoo import api, fields, models


class CRMLead(models.Model):
    _inherit = "crm.lead"

    source_id = fields.Many2one(
        related="partner_id.source_id",
    )
