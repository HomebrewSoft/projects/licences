from odoo import api, fields, models
class SaleOrder(models.Model):
    _inherit = "sale.order"
    has_licence_products = fields.Boolean(
        compute="_compute_has_licence_products",
    )
    contract_count = fields.Integer(
        compute="_compute_contract_ids",
    )
    contract_ids = fields.Many2many(
        comodel_name="contract.contract",
        string="Contracts",
    )
    source_id = fields.Many2one(
        related="partner_id.source_id",
        store=True,
    )

    def _need_manual_invoice(self):
        automatic_invoice = (
            self.env["ir.config_parameter"].sudo().get_param("sale.automatic_invoice")
        )
        return automatic_invoice and self.amount_total == 0
    @api.depends("order_line.product_id.is_licence")
    def _compute_has_licence_products(self):
        for order in self:
            order.has_licence_products = any(
                line.product_id.is_licence for line in order.order_line
            )
    def action_view_contracts(self):
        contracts = self.mapped("contract_ids")
        action = self.env.ref("contract.action_customer_contract").read()[0]
        if len(contracts) > 1:
            action["domain"] = [("id", "in", contracts.ids)]
        elif len(contracts) == 1:
            form_view = [(self.env.ref("contract.contract_contract_customer_form_view").id, "form")]
            action["views"] = form_view
            action["res_id"] = contracts.id
        else:
            action = {"type": "ir.actions.act_window_close"}
        return action
    def _compute_contract_ids(self):
        for order in self:
            order.contract_count = len(order.contract_ids)
